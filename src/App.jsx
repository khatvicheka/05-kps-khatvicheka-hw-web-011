import React, { Component } from "react";
import { Card, Form, Container, Row } from "react-bootstrap";
import ResultList from "./components/ResultList";
import "./style.css";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operator: "+",
      num1: "",
      num2: "",
      results: [],
    };
  }

  handleChange = (event) =>
    this.setState({ [event.target.name]: event.target.value });
  Calculate = () => {
    const { num1, num2, operator } = this.state;

    if (!num1 || !num2) {
      alert("Please input valid number!");
    } else {
      let result;
      switch (operator) {
        case "+":
          result = Number(num1) + Number(num2);
          break;
        case "-":
          result = Number(num1) - Number(num2);
          break;
        case "*":
          result = Number(num1) * Number(num2);
          break;
        case "/":
          result = Number(num1) / Number(num2);
          break;
        case "%":
          result = Number(num1) % Number(num2);
          break;
        default:
          break;
      }
      this.setState({
        results: [
          ...this.state.results,
          {
            result: result,
            num1: this.state.num1,
            num2: this.state.num2,
            operator: this.state.operator,
          },
        ],
      });
    }
  };
  render() {
    return (
      <Container>
        <Row>
          <div className="col-md-4">
          <Card style={{ width: '23rem' }}>
            <Card.Img variant="top" src="https://icons.iconarchive.com/icons/dtafalonso/android-lollipop/512/Calculator-icon.png"/>
              <Card.Body>
                <Form.Control
                  className="form"
                  type="text"
                  name="num1"
                  value={this.state.num1}
                  onChange={this.handleChange}
                />
                <Form.Control
                  className="form"
                  type="text"
                  name="num2"
                  value={this.state.num2}
                  onChange={this.handleChange}
                />
                <Form.Control
                  className="form"
                  as="select"
                  name="operator"
                  onChange={this.handleChange}
                >
                  <option value="+">+ Plus</option>
                  <option value="-">- Subtract</option>
                  <option value="*">* Multiply</option>
                  <option value="/">/ Devide</option>
                  <option value="%">% Modulo</option>
                </Form.Control>
                <button className="btn btn-primary" onClick={this.Calculate}>
                  Calculate
                </button>
              </Card.Body>
            </Card>
          </div>
          <div className="col-md-4">
            <ResultList results={this.state.results} />
          </div>
        </Row>
      </Container>
    );
  }
}
