import React from "react";
import { ListGroup } from "react-bootstrap";

export default function ResultItem(props) {
  const { result, num1, num2, operator } = props.result;
  return (
    <ListGroup.Item key={props.index}>
      {" "}
      {num1} {operator} {num2} = {result}{" "}
    </ListGroup.Item>
  );
}
