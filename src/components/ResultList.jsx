import React from "react";
import { Card, ListGroup } from "react-bootstrap";
import ResultItem from "./ResultOperation";

export default function ResultList(props) {
  return (
    <Card>
      <Card.Header>
        <h5>Calculation History</h5>
      </Card.Header>
      <ListGroup variant="flush">
        {props.results.map((result, index) => (
          <ResultItem key={index} result={result} />
        ))}
      </ListGroup>
    </Card>
  );
}
